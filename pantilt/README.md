# Install

## udev rules

Copy `config/90-dynamixel.rules` in `/etc/udev/rules.d`

Restart `udev` service (`sudo service udev restart`)
or reboot

Check: connect the pantilt/arm device and check the dev file 
`/dev/dinamixel` is present  (note: typo in dinamixel is known).


## docker image

Build and run pantilt docker image (see `docker` folder).
Use the pantilt container to run the ROS nodes.


# Check motors

        roscd dynamixel_driver/scripts
        python info_dump.py -p <PORT> <motor IDs>

e.g.,

        python info_dump.py -p /dev/dinamixel 0 1 2 3 4 5 6 7 8
        

# Configuration

Default configuration is set in `pantilt.yaml` file


# Launch


        cd pantilt/launch
        roslaunch start_dynamixel.launch

# Get joints values

        rostopic echo <controller>/state

Example:

        rostopic echo /pan_controller/state

# Set joints values

        rostopic pub <controller>/command std_msgs/Float64 "data: <target_angle>" --once

Examples:

        rostopic pub /pan_controller/command std_msgs/Float64 "data: 0.0" --once

