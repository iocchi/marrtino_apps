import rospy
import argparse
import sys

from std_msgs.msg import String,Float64
#from sensor_msgs.msg import JointState
from dynamixel_msgs.msg import JointState

NJoints = 5

arm_pubs = None
ee_pub = None
joint_data = [0] * NJoints
ee_data = 0

def jointstate_cb(data):
    global joint_data
    jn = int(data.name[6])
    joint_data[jn] = data.current_pos
    #print(joint_data)

def eestate_cb(data):
    global ee_data
    ee_data = data.current_pos
    #print(ee_data)


def init(nodename='test_arm'):
    global arm_pubs, ee_pub
    rospy.init_node(nodename,  disable_signals=True)
    rospy.sleep(1)
    print("ROS node %s initialized." %nodename)
    arm_pubs = [ rospy.Publisher('/j%d_controller/command' %i, Float64, queue_size=1,   latch=True) for i in range(0,NJoints) ]
    ee_pub = rospy.Publisher('/ee_controller/command', Float64, queue_size=1,   latch=True)
    print("Arm Publishers:")
    print(arm_pubs)

    js_subs = [ rospy.Subscriber('/j%d_controller/state' %i, JointState, jointstate_cb) for i in range(0,NJoints) ]
    ee_sub = rospy.Subscriber('/ee_controller/state', JointState, eestate_cb)

    print("Arm Subsribers:")
    print(js_subs)


def setjoints(targets = [0,0,0,0,0]):
    global arm_pubs
    for p,t in zip(arm_pubs,targets):
        p.publish(t)


def setee(v):
    global ee_pub
    ee_pub.publish(v)
        



if __name__=='__main__':


    parser = argparse.ArgumentParser(description='test arm')

    parser.add_argument('-j', type=float, nargs='+', default=[0, 0, 0, 0, 0])

    parser.add_argument('-ee', type=float, default=None)

    parser.add_argument('--read', action='store_true', default=None)

    args = parser.parse_args()

    init()
        
    if args.read:
        for i in range(100):
            now = rospy.Time.now() 
            print(now)
            rospy.sleep(0.5)
            
            print("Joint state: ")
            print(joint_data)
            print("EE state: ")
            print(ee_data)

    else:
        # targets = [ args.j0, args.j1, args.j2, args.j3, args.j4 ]

        print("Target joints: %r" %args.j)    

        if (len(args.j)!=5):
            print("Expected list of 5 joints")
            sys.exit(1)

        setjoints(args.j)

        if args.ee is not None:
            setee(args.ee)
    
        rospy.sleep(3)
    
    
    

