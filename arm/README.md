# Install

## udev rules

Copy `config/90-dynamixel.rules` in `/etc/udev/rules.d`

Restart `udev` service (`sudo service udev restart`)
or reboot

Check: connect the pantilt/arm device and check the dev file 
`/dev/dinamixel` is present  (note: typo in dinamixel is known).


## docker image

Build and run pantilt docker image (see `docker` folder).
Use the pantilt container to run the ROS nodes.


# Check motors

        roscd dynamixel_driver/scripts
        python info_dump.py -p <PORT> <motor IDs>

e.g.,

        python info_dump.py -p /dev/dinamixel 0 1 2 3 4 5 6 7 8

Note: this does not work when ROS controllers are running.        

# Configuration

Default configuration is set in `config/arm5.yaml` file

# Launch

Defualt for pantilt device

        cd arm/launch
        roslaunch start_dynamixel.launch
        


# Get joints values

        rostopic echo <controller>/state

Example:

        rostopic echo /j0_controller/state
        
        


# Set joints values

        rostopic pub <controller>/command std_msgs/Float64 "data: <target_angle>" --once

Examples:

        rostopic pub /j0_controller/command std_msgs/Float64 "data: 0.0" --once

        rostopic pub /ee_controller/command std_msgs/Float64 "data: 0.0" --once

Use python script:

        python test_arm.py -j <j0> <j1> <j2> <j3> <j4>  -ee <ee>
        
Example: rest position

        python test_arm.py -j 0 0.6 0.8 1.2 0  -ee -1.0

## EE 

Open -1.0
Closed 1.0

