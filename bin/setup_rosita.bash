#!/bin/bash

sudo systemctl stop apt-daily.timer
sudo systemctl disable apt-daily.timer
sudo systemctl stop apt-daily-upgrade.timer
sudo systemctl disable apt-daily-upgrade.timer

sudo bash -c 'echo "SUBSYSTEM==\"bcm2835-gpiomem\", GROUP=\"gpio\", MODE=\"0777\""  > /etc/udev/rules.d/91-gpio.rules'

sudo service udev restart

cd ../docker && docker build --network=host -t marrtino:pantilt -f Dockerfile.pantilt . && cd -

cp ../docker/system_config_rosita.yaml ../system_config.yaml
cp ../start/rosita.yaml ../autostart.yaml

