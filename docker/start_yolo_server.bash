#!/bin/bash

IM="marrtino:yolo"
NVIDIA_STR=""

if nvidia-detector 2> /dev/null; then
  echo "Nvidia runtime"
  NVIDIA_STR="--runtime nvidia --gpus all --ipc=host --ulimit memlock=-1 --ulimit stack=67108864"
  #IM="marrtino:yolo-gpu"
fi

docker run -it -d \
    -u $(id -u):$(id -g)  \
    --name marrtino_yolo --rm \
    --privileged \
    --net=host \
    $NVIDIA_STR \
    -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
    -v $HOME/.Xauthority:/root/.Xauthority:rw \
    -e DISPLAY=$DISPLAY \
    -e QT_X11_NO_MITSHM=1 \
    -e MARRTINO_APPS_HOME=/home/robot/src/marrtino_apps \
    -v $MARRTINO_APPS_HOME:/home/robot/src/marrtino_apps \
    -v $HOME/playground:/home/robot/playground \
    -w /home/robot/src/marrtino_apps \
    $IM

sleep 3

echo "@yolo" | netcat -w 1 localhost 9242
# Image server running on port 9300


# docker exec -it marrtino_yolo tmux a



