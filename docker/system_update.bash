#!/bin/bash

echo "System update started"

source $MARRTINO_APPS_HOME/docker/stop_docker.bash 

sleep 5


cd $MARRTINO_APPS_HOME/docker
git pull
python3 dockerconfig.py    # generates docker-compose files
echo "docker-compose pull"
docker-compose pull
echo "docker build system"
source ./docker_build_system.bash
echo "docker-compose build"
docker-compose build
cd -


# if present use this version
if [ -d $HOME/src/stage_environments ]; then 
  cd $HOME/src/stage_environments
  git pull
  cd docker
  source build.bash
fi


if [ -f /tmp/marrtinosocialon ] && [ "$MARRTINO_SOCIAL" != "" ] && [ -d $MARRTINO_SOCIAL/docker ]; then
  
  cd $MARRTINO_SOCIAL/docker
  git pull
  docker-compose pull
  ./build_docker.bash
  cd -

fi

docker container prune -f
docker image prune -f

touch ~/log/last_systemupdate.log
date >> ~/log/last_systemupdate.log

source $MARRTINO_APPS_HOME/docker/start_docker.bash

echo "System update completed"

