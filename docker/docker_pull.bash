#!/bin/bash

docker pull iocchi/marrtino:base && \
docker pull iocchi/marrtino:teleop && \
docker pull iocchi/marrtino:navigation && \
docker pull iocchi/marrtino:vision && \
docker pull iocchi/marrtino:speech &&
docker pull iocchi/marrtino:objrec

docker tag iocchi/marrtino:base marrtino:base && \
docker tag iocchi/marrtino:teleop marrtino:teleop && \
docker tag iocchi/marrtino:navigation marrtino:navigation && \
docker tag iocchi/marrtino:vision marrtino:vision && \
docker tag iocchi/marrtino:speech marrtino:speech && \
docker tag iocchi/marrtino:objrec marrtino:objrec

