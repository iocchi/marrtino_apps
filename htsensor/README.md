Install Python lib Adafruit_DHT:

    pip install Adafruit_DHT

Enable access to GPIO device

    sudo chmod a+rwx /dev/gpio*
    
Run demo script

    python htsensor.py
    
Run ROS node

    python htsensor_node.py


