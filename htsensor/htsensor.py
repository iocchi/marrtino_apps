# -*- coding: utf-8 -*-

try:
    import Adafruit_DHT
except:
    print("pip install Adafruit_DHT")

import time

sensor = Adafruit_DHT.DHT11
pin = 12

def read_values():
    h,t = Adafruit_DHT.read_retry(sensor,pin)
    return h,t


if __name__=='__main__':

    run = True
    while run:
        try:
            h,t = read_values()
            print("Temperature: %8.3f - Humidity: %8.3f" %(t,h))
            time.sleep(2)
        except KeyboardInterrupt:
            print("User quit")
            run = False

