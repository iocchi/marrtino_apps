# -*- coding: utf-8 -*-

try:
    import Adafruit_DHT
except:
    print("pip install Adafruit_DHT")

import time
import rospy
import htsensor

PARAM_HUMIDITY = 'dht/humidity'
PARAM_TEMPERATURE = 'dht/temperature'

if __name__=='__main__':

    nodename = 'htsensor'
    rospy.init_node(nodename,  disable_signals=True)
    print("ROS node %s started." %nodename)
    print("ROS params: %s %s" %(PARAM_HUMIDITY,PARAM_TEMPERATURE))

    run = True
    while run and not rospy.is_shutdown():
        try:
            rospy.set_param(PARAM_HUMIDITY, 0)
            rospy.set_param(PARAM_TEMPERATURE, 0)
            run = False
        except KeyboardInterrupt:
            print("User quit")
            run = False
        except TypeError:
            rospy.sleep(2)

    run = True
    while run and not rospy.is_shutdown():
        try:
            h,t = htsensor.read_values()
            rospy.set_param(PARAM_HUMIDITY, h)
            rospy.set_param(PARAM_TEMPERATURE, t)
            rospy.sleep(0.5)
        except KeyboardInterrupt:
            print("User quit")
            run = False
        except TypeError:
            rospy.sleep(2)

